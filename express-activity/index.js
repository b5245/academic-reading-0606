const express = require('express');

const app = express();

const port = 4001;

app.use(express.json());

app.use(express.urlencoded({extended:true}));

let users = [];

app.get('/', (req, res) => {
    res.send(users)
})

app.post("/signup", (req, res) => {
    console.log(req.body);

    if(req.body.username !== '' && req.body.password !== ''){
        users.push(req.body);
        res.send(`User ${req.body.username} has successfully registered!`);
    }
    else{
        res.send(`Please input both username and password`);
    }
});

app.post("/login", (req, res) => {
    console.log(req.body)
    for(let i = 0; i < users.length; i++){
        if(req.body.username == users[i].username && req.body.password == users[i].password){
            res.send(`You have logged in successfully!`)
        }
        else{
            res.send(`Login failed! Please sign up first!`)
        }
    }
})

app.listen(port, () => console.log(`Server running at port ${port}`))